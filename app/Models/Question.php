<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = [];
    // protected $with = ['user', 'category'];

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    // Một câu hỏi chỉ có một category
    public function category() {
        return $this->belongsTo(Category::class);
    }
    // Một câu hỏi  có nhiều reply
    public function replies() {
        return $this->hasMany(Reply::class);
    }

    public function getRouteKeyName(){
        return 'slug';
    }

    public function getPathAttribute(){
        return asset("/api/questions/$this->slug");
    }
}
