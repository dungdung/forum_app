<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateQuetionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|string|min:6'
        ];
    }
    // ghi đè messages
    public function messages()
    {
        return [
            'required'=>':attribute khoon ',
            'min'=>':attribute it nhat 6 ki tu '
        ];
    }
    //ghi đè attributes
    public function attributes()
    {
        return [
            'title'=>'tieeu dde ',
        ];
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), 422));
    }
}
